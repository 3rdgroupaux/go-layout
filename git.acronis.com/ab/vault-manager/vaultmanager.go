package main

import (
    "fmt"
    "git.acronis.com/ab/go-service/log"
    uuid "github.com/satori/go.uuid"
)

func main() {
    log.GetLogger()
    u, _ := uuid.NewV4()
    fmt.Println(u.String())
}
