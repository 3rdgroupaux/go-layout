package log

// Logger used to wrap default "log" for 3rd-party components
type Logger struct {
	format string
}

// GetLogger returns wrapper for default "log"
func GetLogger() *Logger {
	return nil
}

// Debug logs a message at level Debug.
func (l *Logger) Debug(message string, args ...interface{}) {



}
